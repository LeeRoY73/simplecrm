export interface ITableComponentProps {
    headerData: string[]
    bodyKeys: string[]
    bodyData: Array<any>
}
