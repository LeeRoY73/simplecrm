import './tableComponent.scss'
import {ITableComponentProps} from "./tableComponentProps";



const TableComponent = (props: ITableComponentProps) => {
    const {
        headerData,
        bodyKeys,
        bodyData
    } = props;

    return (
        <div className="table-component">
            <table>
                <thead>
                <tr>
                    {
                        headerData.map((headerItem, index) => {
                            return <th scope="col" key={index}>
                                <h4>
                                    {headerItem}
                                </h4>
                            </th>
                        })
                    }
                </tr>
                </thead>

                <tbody>
                {
                    bodyData.map((bodyList, index) => {
                        return (
                            <tr key={index}>
                                {
                                    bodyKeys.map((key, index) => {
                                        return <td data-label="Account" key={index}>{bodyList[key]}</td>
                                    })
                                }
                            </tr>
                        )
                    })
                }
                </tbody>
            </table>
        </div>
    )
}

export default TableComponent
