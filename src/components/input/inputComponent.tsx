import './inputComponent.scss'
import {IInputComponentProps} from "./inputComponentProps";


const InputComponent = (props:IInputComponentProps) => {
    const {type, placeholder} = props
    return (
            <div className="input">
                <div className="input-container">
                    <input type={type} className="input-field" placeholder={placeholder}/>
                </div>
            </div>

    )
}
export default InputComponent
