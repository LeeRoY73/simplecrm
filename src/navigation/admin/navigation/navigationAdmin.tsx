import Menu from '../menu/menu';
import TopBar from '../topBar/topBar';
import './navigationAdmin.scss'

const NavigationAdmin = () => {

    return (
        <div className="navigation-admin">
            <TopBar/>
        </div>
    )
}

export default NavigationAdmin