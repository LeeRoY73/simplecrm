import React from "react";
import {useLocation, useNavigate} from "react-router-dom";
import {getRouterByRoleAndName} from "../../../router/config";
import {Button} from "@mui/material";

interface IMenuItem {
    view: {
        name: string;
        patch: string;
    }
    key:number;
}

const MenuItem = (props: IMenuItem) => {
    const {view,key} = props
    const useNavigateTest = useNavigate();
    const location = useLocation();

    const GoToView = (name: string) => {
        useNavigateTest(getRouterByRoleAndName('admin', name).path)
    }

    const isTabActive = getRouterByRoleAndName('admin', view.patch).path === location.pathname
    const classNameLi = isTabActive ? 'menu-block-item active-menu-tab' : 'menu-block-item'

    return (
        <>
            <li className={classNameLi} key={key} onClick={() => GoToView(view.patch)}>
                <div className="menu-block-item-container">
                    <Button variant="text"> {view.name}</Button>
                </div>
            </li>

        </>

    )
}
export default MenuItem;