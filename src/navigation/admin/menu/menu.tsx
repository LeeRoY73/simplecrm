import React from 'react';
import {Outlet} from 'react-router-dom';

import './menu.scss'
import MenuItem from "./menuItem";

const Menu = () => {
    const views = [{
            name: "Товары",
            patch: "product",
        },
        {
            name: "Юзеры",
            patch: "user",
        },]

    return (
        <div className="menu">
            <ul className="menu-block">
                <div className="menu-block-logo">
                    <h5>
                        <a className="menu-block-logo-text" href="">
                            ИП "Рога и копыта"
                        </a>
                    </h5>
                </div>

                {
                    views.map((value, index) => {
                        return <MenuItem view={value} key={index}/>
                    })
                }
            </ul>

            <main>
                <Outlet/>
            </main>
        </div>

    )
}
export default Menu