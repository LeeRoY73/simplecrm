import React from 'react'
import {Outlet} from 'react-router-dom';
import './topBar.scss'
import logo from '../../../assets/img/logo.png'
import TopBarItem from "./topBarItem";
import {getChildFullPatch, getRouterByRoleAndName} from "../../../router/config";

const TopBar = () => {
    const topBarItems = [
        {
            name: 'main',
            title: 'Главная',
            path: getRouterByRoleAndName('admin', 'main').path
        },
        {
            name: 'user',
            title: 'Магазины',
            path: '/'
        },
        {
            name: 'product',
            title: 'Товары',
            path: getChildFullPatch('admin', 'product', 'products'),
        },
        {
            name: 'user',
            title: 'Услуги',
            path: '/'
        }
    ]


    return (
        <>
            <div className="top-bar">
                <div className="top-bar-block">
                    <img className="top-bar-block__logo" src={logo}/>
                    <div className="top-bar-block__main">

                        <div className="top-bar-block__main-link-block">
                            {
                                topBarItems.map(item => {
                                    return (
                                        <TopBarItem title={item.title} name={item.name} path={item.path}/>
                                    )
                                })
                            }
                        </div>


                    </div>
                    <div className="top-bar-block__exit-block">
                        <a className="top-bar-block__text">
                            Выход
                        </a>
                    </div>
                </div>
            </div>
            <main>
                <div className="top-bar-main-block">
                    <Outlet/>
                </div>
            </main>
        </>

    )
}

export default TopBar