import React from "react";
import {useLocation, useNavigate} from "react-router-dom";
import {getParentPatchByFullChildPatch, getRouterByRoleAndName} from "../../../router/config";

interface ITopBarItem {
    name: string;
    title: string;
    path: string
}

const TopBarItem = (props: ITopBarItem) => {
    const {name = '', title = '', path = ''} = props
    const useNavigateTest = useNavigate();
    const location = useLocation();

    const isTabActive = getParentPatchByFullChildPatch(location.pathname)?.path === getRouterByRoleAndName('admin', name).path
    const classNameModification = isTabActive ? 'top-bar-block-text top-bar-block-text__active' : 'top-bar-block-text'
    const GoToView = () => {
        useNavigateTest(path)
    }

    return (
        <a className={classNameModification} onClick={() => GoToView()}>
            {title}
        </a>
    )
}

export default TopBarItem