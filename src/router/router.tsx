import React from "react";
import {createBrowserRouter} from "react-router-dom";

import NavigationAdmin from "../navigation/admin/navigation/navigationAdmin";
import Users from "../pages/admin/Users";
import Post from "../pages/admin/Post";
import Products from "../pages/admin/product/products/products"

import PageNotFound from "../pages/pageNotFound/pageNotFound";
import {getChildFullPatch, getRouterByRoleAndName} from "./config";
import Subcategory from "../pages/admin/product/subcategory/subcategory";
import Category from "../pages/admin/product/category/category";
import Main from "../pages/admin/main/main";

const router = createBrowserRouter([
    {
        path: '/',
        children: [
            {
                path: '/admin',
                element: <NavigationAdmin/>,
                children: [
                    {
                        path: getRouterByRoleAndName('admin', 'main').path,
                        element: <Main/>,
                    },
                    {
                        path: getRouterByRoleAndName('admin', 'post').path,
                        element: <Post/>,
                    },
                    {
                        path: getRouterByRoleAndName('admin', 'user').path,
                        element: <Users/>,
                    },
                    {
                        path: getRouterByRoleAndName('admin', 'user').path,
                        element: <Users/>,
                    },
                    {
                        path: getChildFullPatch('admin', 'product', 'products'),
                        element: <Products/>,
                    },
                    {
                        path: getChildFullPatch('admin', 'product', 'subcategories'),
                        element: <Subcategory/>,
                    },
                    {
                        path: getChildFullPatch('admin', 'product', 'categories'),
                        element: <Category/>,
                    },
                ]
            },
        ]
    },
    {
        path: '*',
        element: <PageNotFound/>
    }


]);

export default router