import config from './config.json'

interface IRoutes {
    admin: Array<IRouteItem>
}

interface IRouteItem {
    name: string;
    path: string;
    children?: Array<IRouteItemChildren>
}

interface IRouteItemChildren {
    name: string;
    path: string;
}


const routers: IRoutes = config.routers

export const getRoutersByRole = (role: string) => {

    return routers[role as keyof typeof routers]
}
export const getRouterByRoleAndName = (role: string, name: string) => {

    const selectRoutersByRole = routers[role as keyof typeof routers]
    const routerByRoleAndName = selectRoutersByRole.find(router => {
        return router.name === name
    })

    return routerByRoleAndName ?? {path: '/error', name: 'error', children: []}
}

export const getParentPatchByFullChildPatch = (patch: string) => {
    return routers.admin.find(routerItem => {
        const routerItemPatch = routerItem.path

        if(routerItem.children) {
            return routerItem.children?.find(routerChildItem => {

                return routerItemPatch + '/' + routerChildItem.name === patch
            })
        }else {
            return routerItemPatch === patch
        }
    })
}

export const getChildFullPatch = (role: string, name: string, childName: string) => {

    const selectRoutersByRole = routers[role as keyof typeof routers]
    const routerByRoleAndName = selectRoutersByRole.find(router => {
        return router.name === name
    })
    let childrenRouter = {path: '/error', name: 'error'}

    if (routerByRoleAndName?.children) {
        childrenRouter = routerByRoleAndName.children.find(router => {
            return router.name === childName
        }) ?? {path: '/error', name: 'error'}
    }

    return routerByRoleAndName?.path + childrenRouter.path
}

export default {getRoutersByRole, getRouterByRoleAndName};