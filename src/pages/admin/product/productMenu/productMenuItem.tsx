import {useLocation, useNavigate} from "react-router-dom";
import {getChildFullPatch} from "../../../../router/config";

interface IProductMenuItem {
    title: string;
    name: string;
    key: number
}

const ProductMenuItem = (props: IProductMenuItem) => {
    const {title = '', name = '', key} = props
    const useNavigateTest = useNavigate();
    const location = useLocation();

    const GoToView = (name: string) => {
        useNavigateTest(getChildFullPatch('admin', 'product', name))
    }

    const isTabActive = getChildFullPatch('admin', 'product', name) === location.pathname
    const className = isTabActive ? 'product-menu-item product-menu-item__active' : 'product-menu-item'

    return (
        <a key={key} className={className} onClick={() => GoToView(name)}> {title} </a>
    )
}
export default ProductMenuItem;