import './productMenu.scss'
import ProductMenuItem from "./productMenuItem";

const ProductMenu = () => {
    const productItems = [
        {
            title: 'Товары',
            name: 'products'
        },
        {
            title: 'Подкатегории',
            name: 'subcategories'
        },
        {
            title: 'Категории',
            name: 'categories'
        },
    ]

    return (
        <div className="product-menu">
            {
                productItems.map(((productItem, key) => {
                    return (
                        <ProductMenuItem
                            title={productItem.title}
                            name={productItem.name}
                            key={key}
                        />)
                }))
            }
        </div>
    )
}
export default ProductMenu;