import './products.scss'
import TableComponent from '../../../../components/table/tableComponent'
import product from '../../../../mocks/product.json'
import ProductMenu from "../productMenu/productMenu";
import InputComponent from "../../../../components/input/inputComponent";
const Products = () => {
    const headerData = [
        'id',
        'имя',
        'описания',
        'активный',
        'магазин',
        'категория',
        'подкатегория'
    ]
    const bodyKeys = [
        'id',
        'name',
        'description',
        'isActiveShow',
        'shop',
        'category',
        'subcategory'
    ]
    const bodyData = product

    return (
        <div className="products">

            <ProductMenu/>
            <div className="products-card">
                <h5 className="products-filter-title">
                    Фильтры
                    <hr/>
                </h5>

                <div className="products-filter">
                    <div className="products-filter-item">
                        <InputComponent type='text' placeholder='ID'/>
                    </div>

                    <div className="products-filter-item">
                        <InputComponent type='text' placeholder='Имя'/>
                    </div>

                    <div className="products-filter-item">
                        <InputComponent type='text' placeholder='Описание'/>
                    </div>

                    <div className="products-filter-item">
                        <InputComponent type='text' placeholder='Активный'/>
                    </div>

                    <div className="products-filter-item">
                        <InputComponent type='text' placeholder='Магазин'/>
                    </div>

                    <div className="products-filter-item">
                        <InputComponent type='text' placeholder='Категория'/>
                    </div>

                    <div className="products-filter-item">
                        <InputComponent type='text' placeholder='Подкатегория'/>
                    </div>
                </div>
            </div>


            <div className="products-card">
                <div className="products-table-scroll">
                    <TableComponent headerData={headerData} bodyKeys={bodyKeys}
                                    bodyData={bodyData}/>
                </div>
            </div>
        </div>
    )
}

export default Products
