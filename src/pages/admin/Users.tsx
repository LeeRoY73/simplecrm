import React from 'react'
import {Link, useNavigate} from 'react-router-dom'

const Users = () => {
    let navigate = useNavigate();
    const handleBack = () => {
        navigate(-1);
    };

    return (
        <div>
            <h1>Welcome to Users route </h1>

            <button onClick={handleBack}> link test </button>

            <Link to="/admin/post">
                <p>go to users page</p>
            </Link>
        </div>
    )
}

export default Users
