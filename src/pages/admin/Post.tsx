import React from 'react'
import {Link} from "react-router-dom";

const Post = () => {

    return (
        <div>
            <h1>Welcome to Posts route</h1>
            <Link to="/admin/user">
                <p>go to users page</p>
            </Link>
        </div>
    )
}

export default Post
